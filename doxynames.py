#!/usr/bin/env python
import sys
from lxml import etree
import random
import bisect

class RunningMeanStdDev:

    def __init__(self):
        self.M=0.
        self.S=0.
        self.count=0

    def addItem(self, item):
        self.count+=1
        oldM=self.M
        self.M+=(item-oldM)/self.count
        if self.count>1:
            self.S+=(item-oldM)*(item-self.M)

    def getMean(self):
        return self.M

    def getStdDev(self):
        return (self.S/(self.count-1))**0.5

    def getRandom(self):
        return random.gauss(self.getMean(), self.getStdDev())

class WordsClassifier:

    def __init__(self):
        self.wordsBag={}
        self.meanLength = RunningMeanStdDev()
        self.meanUnderscority = RunningMeanStdDev()
        self.weightsList=None

    def addIdentifier(self, identifier):
        r = splitName(identifier)
        wordList=r['pieces']
        self.meanLength.addItem(len(wordList))
        self.meanUnderscority.addItem(float(r['underscoresSplits'])/len(wordList))
        for w in wordList:
            if w not in self.wordsBag:
                self.wordsBag[w]=0
            self.wordsBag[w]+=1
        self.weightsList=None


    def buildWeightsList(self):
        if self.weightsList:
            return;
        self.weightsList=[]
        c=0
        for k,v in self.wordsBag.iteritems():
            c+=v
            self.weightsList.append((c, k))

    def pickWord(self):
        self.buildWeightsList()
        rnd = random.random() * self.weightsList[-1][0]
        return self.weightsList[bisect.bisect_right(self.weightsList, (rnd,''))][1]

    def randomIdentifier(self):
        ret=[]
        l=max(self.meanLength.getRandom(),1)
        nextTitleCase=False
        m=int(l+0.5)
        for i in range(m):
            w=self.pickWord()
            if nextTitleCase:
                w=w.title()
            ret.append(w)
            if self.meanUnderscority.getRandom()>1.:
                if i != m-1:
                    ret.append('_')
            else:
                nextTitleCase=True;
        return ''.join(ret)


def splitName(name):
    underscorePieces = name.split("_")
    pieces = []
    underscoresSplits=0
    camelCaseSplits=0
    for p in underscorePieces:
        if len(p)==0:
            continue
        underscoresSplits+=1
        cur = []
        for i in range(len(p)):
            last = i == len(p)-1
            upper=p[i].isupper()
            nextUpper=True if last else p[i+1].isupper()
            if p[i].isupper() and not nextUpper and len(cur):
                pieces.append(''.join(cur))
                cur = []
            cur.append(p[i])
            if p[i].islower() and nextUpper:
                pieces.append(''.join(cur))
                cur = []

        if len(cur):
            pieces.append(''.join(cur))

    return {"pieces": pieces, "underscoresSplits": underscoresSplits}


classes=WordsClassifier()
members=WordsClassifier()

tree = etree.parse(sys.argv[1], parser=etree.XMLParser(encoding="ISO-8859-2"))
for it in tree.findall("./compound"):
    if it.attrib['kind']!='struct' and it.attrib['kind']!='class':
        continue
    classes.addIdentifier(it.find('name').text.split(':')[-1])
    for mit in it.findall('member'):
        members.addIdentifier(mit.find('name').text)

for _ in range(100):
    print classes.randomIdentifier()
    for __ in range(10):
        print "\t",members.randomIdentifier()
    print ""
