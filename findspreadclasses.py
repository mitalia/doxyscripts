from glob import glob
from lxml import etree

totbf=[]
for f in glob("class*.xml"):
    try:
        tree = etree.parse(f, parser=etree.XMLParser(encoding="ISO-8859-2"))
        name=tree.find('./compounddef/compoundname').text
        decfiles=set()
        bodyfiles=set()
        for it in tree.findall('./compounddef/sectiondef/memberdef'):
            loc=it.find('location')
            decfiles.add(loc.attrib['file'])
            if 'bodyfile' in loc.attrib:
                bodyfiles.add(loc.attrib['bodyfile'])
            else:
                print "No bodyfile in", it.find('definition').text
        totbf.append((name, list(bodyfiles)))

    except Exception as ex:
        print "Exception parsing file", f
        print repr(ex)

totbf=[i for i in totbf if len(i[1])>2]
totbf.sort(key=lambda l: len(l[1]))
for i in totbf:
    print i[0]
    for j in i[1]:
        print "\t", j

